import React, { Component } from 'react';
import {  BrowserRouter } from 'react-router-dom';
import './App.css';
import NavigationBar from './components/navigationBar/NavigationBar';


class App extends Component {
  
  render() {   
    return (
      <div className="App">
        <BrowserRouter><NavigationBar /></BrowserRouter>
      </div>
    );
  }
}

export default App;