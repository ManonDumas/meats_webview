import React, { Component } from 'react';
import './Home.scss'

class Home extends Component {
  
  render() {   
    return (
      <div className="Home">
          <h1>Work in Progress</h1>
          <img className="rotate" src={require('./loading_logo.png')}/>
        <p>In this web app, you can buy video games.</p>
      </div>
     
    );
  }
}

export default Home;