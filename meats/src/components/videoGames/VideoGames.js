import React, { Component } from 'react';
import VideoGameCard from "./VideoGameCard";
import { displayVideoGames } from '../../service/VideoGamesService';

class VideoGames extends Component {
  state = {
    videoGames: []
  }

  async componentDidMount() {
    this.displayVideoGamesService();
  }

  async displayVideoGamesService(){
    const response = await displayVideoGames();
    const data = await response.json();
    this.setState({ videoGames: data });
  }

  render() {   
    return (
      <div>
        <h1>Video Games list</h1>        
        <VideoGameCard videoGames={this.state.videoGames} />
      </div>
    );
  }
}

export default VideoGames;