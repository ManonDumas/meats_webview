import React from 'react';
import { Button } from 'reactstrap';
import './VideoGameCard.scss'

const VideoGames = ({ videoGames }) => {
  return (
    <div className="cards">
      {videoGames.map((videoGame) => (
        <div className="cards-content">
          <div className="cards-content-text"><span>Name : </span>{videoGame.name_video_game}</div>
          <div className="cards-content-text"><span>Company : </span>{videoGame.name_company}</div>
          <div className="cards-content-text"><span>Price : </span>{videoGame.price}</div>
          <Button className="button">Add to cart</Button>
        </div>
      ))}
    </div>
  )
};

export default VideoGames;