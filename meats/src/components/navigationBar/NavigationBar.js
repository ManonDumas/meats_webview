import React, { Component } from 'react';
import { Navbar, Nav } from 'reactstrap';
import { Route, Redirect, NavLink, Switch } from 'react-router-dom';
import './NavigationBar.scss';
import Home from '../home/Home';
import VideoGames from '../videoGames/VideoGames';


class NavigationBar extends Component {

  render() {
    const tabs=['Home', 'Video Games'];   
    return (
      <div>
        <Navbar navbar>
          <div className="app_name">meatS</div>
          <Nav>
            <ul>
            {tabs.map((tab)=> (
              <li><NavLink className="tabs_style" exact
              activeStyle={{ color: '#FFC107' }} to={`/${tab}`}>{tab}</NavLink></li>
              ))}
            </ul>
          </Nav>
        </Navbar>
        <div className="Routing">
          <Switch>
            <Route path='/Home' exact component={Home} />
            <Route path='/Video Games' component={VideoGames} />
            <Redirect to='/Home' />
          </Switch> 
      </div>
    </div>
    );
  }
}

export default NavigationBar;